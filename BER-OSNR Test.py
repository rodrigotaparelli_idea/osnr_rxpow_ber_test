#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Copyright Idea! - Electronic Systems
#   _____     _               _
#  |_   _|   | |             | |
#    | |   __| |  ___   __ _ | |
#    | |  / _` | / _ \ / _` || |
#   _| |_| (_| ||  __/| (_| ||_|
#  |_____|\__,_| \___| \__,_|(_)
# 
# Name:         Sensitivity
# Purpose:      OSNR step change in order to map RX Sensitivity 
#               by BER/post-FEC
# Created By  : Rodrigo Taparelli - rodrigo.taparelli@idea-ip.com
# Created Date: 27 09 2021
# Release:      1.0.0
#               1.0.1 - Added set NOISE att. to 40dB at the end of routine
# =============================================================================

import pyvisa
import time
import numpy as np
from Keysight_N7752A_Driver import *
from yokogawa import *
import pickle
import pandas as pd 
import datetime
import idea_types
import cfp_api.cfp_api as cfp_api 
import matplotlib.pyplot as plt

#==================USER inputs==================================================================================
# VOA - Variable Optical Noise Attenuator - Test Params
NOISE_VOA_attenuation_ch_1_2_start = 6.0     
NOISE_VOA_attenuation_ch_1_2_end = 0.0      
NOISE_VOA_attenuation_ch_1_2_inc = -1.00      

RX_VOA_attenuation_ch_3_4_start = 8.0

BER_trigger_value_to_change_step_size = 1e-3
New_RX_NOISE_VOA_attenuation_step = -0.1
#================================================================================================================

# CFP/CFP2 definitions=======================================================================================================
HOSTBOARD_SLOT = 3    # Host Board Slot 
#================================================================================================================

#==================Equipment Addresss/Definitions================================================================
#N7752A Equipment
N7752A_VISA = openInst('TCPIP0::10.24.101.131::inst0::INSTR')
Noise_VOA_Instrument = N7752A(N7752A_VISA,1)
Noise_Power_Monitor_Instrument = N7752A(N7752A_VISA,2)
Rx_VOA_Instrument = N7752A(N7752A_VISA,3)
Rx_Power_Monitor_Instrument = N7752A(N7752A_VISA,4)

#OSA Yokogawa Equipment
YOKOGAWA_AQ6370_IP = '10.24.101.130'
YOKOGAWA_AQ6370_PORT = 10001
YOKOGAWA_AQ6370_USER = 'anonymous'
YOKOGAWA_AQ6370_PWD = ''

#eq and cfp connection=====================================
cfp_obj=cfp_api.CfpApi(hostbrd_slot=HOSTBOARD_SLOT)
print('CFP serial number:')
print(cfp_obj.cfp_serial)
print('CFP Model:')
print(cfp_obj.cfp_model)
Osa_Instrument = Osa(YOKOGAWA_AQ6370_IP, YOKOGAWA_AQ6370_PORT,YOKOGAWA_AQ6370_USER,YOKOGAWA_AQ6370_PWD)      


#Initial configuration values=======================
Noise_VOA_Instrument.setAtten(NOISE_VOA_attenuation_ch_1_2_start) #need to check if needed
Rx_VOA_Instrument.setAtten(RX_VOA_attenuation_ch_3_4_start)
time.sleep(1)
#=================================================================================================================

#BER versus receiver sensitivity measurements test main===========================================================        

#list definitions
Noise_Attenuation_Read_Value=[]
OSA_OSNR_Read_Value=[]  
BER_A_Read_Value=[]
Post_FEC_A_Read_Value=[]
BER_B_Read_Value=[]
Post_FEC_B_Read_Value=[]
  
print('BER versus OSNR measurements') 
NOISE_VOA_attenuation_Range=np.arange(NOISE_VOA_attenuation_ch_1_2_start,NOISE_VOA_attenuation_ch_1_2_end+NOISE_VOA_attenuation_ch_1_2_inc,NOISE_VOA_attenuation_ch_1_2_inc)   
 
timeout=10 #10seconds for command timeout

attenuation=0
OSNR=0
BER_A=0
BER_B=0
postFEC_A=0
postFEC_B=0

OSA_OSNR_Read_Value_temp=[]
Osa_Instrument.set_sweep_mode('single')
for NOISE_VOA_attenuation_set in NOISE_VOA_attenuation_Range:
    
    t0 = time.time()
      
    Noise_VOA_Instrument.setAtten(NOISE_VOA_attenuation_set) #step by step attenuation
    opcVal = Noise_VOA_Instrument.getOPC()                          #Uses *OPC?
    td = time.time()-t0
    while (opcVal == 0) and (td < timeout):
        opcVal =Noise_VOA_Instrument.getOPC()
        td = time.time()-t0
        time.sleep(0.1)

    if td >= timeout:       
        testtime = td
        print("1-Timeout waiting for *OPC? to return 1.  td = %0.2f, OPC = %d"%(td, opcVal))
            
    
    time.sleep(2.0) #  2 seconds delay
   
    Osa_Instrument.start_sweep()
    Osa_Instrument.check_operation()
    
    Noise_Attenuation_Read_Value.append(Noise_VOA_Instrument.getAtten())  #read RX Power by N7752A
    
    OSA_OSNR_Read_Value_temp=Osa_Instrument.get_osnr()
    # print(OSA_OSNR_Read_Value_temp)
    OSA_OSNR_Read_Value.append(OSA_OSNR_Read_Value_temp[-1])
    
     #BER and PosfeC Read-------------------------------
    BER_A_Read_Value.append(cfp_obj.corrected_hdfec_ber())
    BER_B_Read_Value.append(cfp_obj.corrected_hdfec_ber_b())
    Post_FEC_A_Read_Value.append(cfp_obj.post_hdfec_ber())
    Post_FEC_B_Read_Value.append(cfp_obj.post_hdfec_ber_b())
        
    #(attenuation, OSNR, BER, post-FEC errors)
    attenuation=Noise_Attenuation_Read_Value[-1]
    OSNR=OSA_OSNR_Read_Value[-1]
    BER_A=BER_A_Read_Value[-1]
    BER_B=BER_B_Read_Value[-1]
    postFEC_A=Post_FEC_A_Read_Value[-1]
    postFEC_B=Post_FEC_B_Read_Value[-1]

    st1 = f"Att={attenuation}dB"
    st2 = f"OSNR={OSNR:.3e}dB"
    st3 = f"BER_A={BER_A:.3e}"
    st4 = f"post-FEC_BER_A={postFEC_A:.3e}"
    st5 = f"BER_B={BER_B:.3e}"
    st6 = f"post-FEC_BER_B={postFEC_B:.3e}"
    separator = ", "
    info = separator.join([st1,st2,st3,st4,st5,st6])
    print(info)
    time.sleep(1)
    # new loop in order get more points after trigger BER is achieved
    if ((BER_A >= BER_trigger_value_to_change_step_size) or (BER_B >= BER_trigger_value_to_change_step_size)) and (NOISE_VOA_attenuation_set > NOISE_VOA_attenuation_ch_1_2_end):    
       
        NOISE_VOA_attenuation_Range_2=np.arange(NOISE_VOA_attenuation_set+New_RX_NOISE_VOA_attenuation_step,
                                                NOISE_VOA_attenuation_set+NOISE_VOA_attenuation_ch_1_2_inc,
                                                New_RX_NOISE_VOA_attenuation_step)
       
        for NOISE_VOA_attenuation_set_2 in NOISE_VOA_attenuation_Range_2:
            t0 = time.time()
              
            Noise_VOA_Instrument.setAtten(NOISE_VOA_attenuation_set_2) #step by step attenuation
            opcVal = Noise_VOA_Instrument.getOPC()                          #Uses *OPC?
            td = time.time()-t0
            while (opcVal == 0) and (td < timeout):
                opcVal =Noise_VOA_Instrument.getOPC()
                td = time.time()-t0
                time.sleep(0.1)
        
            if td >= timeout:       
                testtime = td
                print("1-Timeout waiting for *OPC? to return 1.  td = %0.2f, OPC = %d"%(td, opcVal))
                    
            
            time.sleep(2.0) # colocar opc
           
            Osa_Instrument.start_sweep()
            Osa_Instrument.check_operation()
            
            Noise_Attenuation_Read_Value.append(Noise_VOA_Instrument.getAtten())  #read RX Power by N7752A
            
            OSA_OSNR_Read_Value_temp=Osa_Instrument.get_osnr() #return is a array and last value it is OSNR
            # print(OSA_OSNR_Read_Value_temp)
            OSA_OSNR_Read_Value.append(OSA_OSNR_Read_Value_temp[-1])
            
            #BER and PosfeC Read-------------------------------
            BER_A_Read_Value.append(cfp_obj.corrected_hdfec_ber())
            BER_B_Read_Value.append(cfp_obj.corrected_hdfec_ber_b())
            Post_FEC_A_Read_Value.append(cfp_obj.post_hdfec_ber())
            Post_FEC_B_Read_Value.append(cfp_obj.post_hdfec_ber_b())          
                
            #(attenuation, OSNR, BER, post-FEC errors)
            attenuation=Noise_Attenuation_Read_Value[-1]
            OSNR=OSA_OSNR_Read_Value[-1]
            BER_A=BER_A_Read_Value[-1]
            BER_B=BER_B_Read_Value[-1]
            postFEC_A=Post_FEC_A_Read_Value[-1]
            postFEC_B=Post_FEC_B_Read_Value[-1]
        
            st1 = f"Att={attenuation}dB"
            st2 = f"OSNR={OSNR:.3e}dB"
            st3 = f"BER_A={BER_A:.3e}"
            st4 = f"post-FEC_BER_A={postFEC_A:.3e}"
            st5 = f"BER_B={BER_B:.3e}"
            st6 = f"post-FEC_BER_B={postFEC_B:.3e}"
            separator = ", "
            info = separator.join([st1,st2,st3,st4,st5,st6])
            print(info)
            time.sleep(1)    
            #check postFEC
            if (postFEC_A > 0) or (postFEC_B > 0):
                break
        #check postFEC
        if (postFEC_A > 0) or (postFEC_B > 0):
            print('Acquisition stoped due post-FEC BER trigger')
            break



    
#round for 3 digits precision scientific notation
OSA_OSNR_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in OSA_OSNR_Read_Value]
BER_A_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in BER_A_Read_Value]
Post_FEC_A_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in Post_FEC_A_Read_Value]
BER_B_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in BER_B_Read_Value]
Post_FEC_B_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in Post_FEC_B_Read_Value]
# #Dictionary
#(attenuation, OSNR, BER, post-FEC errors)
Acquired_data = {
'Att [dB]': Noise_Attenuation_Read_Value,
'OSNR [dB]': OSA_OSNR_Read_Value_R,
'BER A': BER_A_Read_Value_R,
'Post-FEC BER A': Post_FEC_A_Read_Value_R,
'BER B': BER_B_Read_Value_R,
'Post-FEC BER B': Post_FEC_B_Read_Value_R,
}
#print(Acquired_data)
 
# Store data as .cs and .pkl (Year month day time BER-RxPow-Data)
now = datetime.datetime.now()
dt_string = now.strftime("%Y%m%d-%H%M%S")
#print("date and time =", dt_string)

with open("./log/"+dt_string+"_BER-OSNR-Data.pkl", 'wb') as handle:
    pickle.dump(Acquired_data, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
with open("./log/"+dt_string+"_BER-OSNR-Data.pkl", "rb") as f:
    object = pickle.load(f)
    
df = pd.DataFrame(object)
df.to_csv("./log/"+dt_string+"_BER-OSNR-Data.csv", index=False)
    
# BER vs OSNR graph plot
fig, axs = plt.subplots(1, 1)
# axs.title('RxPow  BER graph')

axs.set_xlabel('OSNR (dB)')
axs.set_ylabel('Corrected BER')
axs.grid(True)
axs.semilogy(OSA_OSNR_Read_Value,BER_A_Read_Value, '.-', label='Corrected BER A')
axs.plot(OSA_OSNR_Read_Value, BER_B_Read_Value, '.-', label='Corrected BER B')
axs.legend(loc='upper right')
plt.title(f"Module #{cfp_obj.cfp_serial}: OSNR x Corrected BER") 
plt.savefig("./log/"+dt_string+"_BER-OSNR-Graph" + '.png')

#Set NOISE VOA to max attenuation 40dB
Noise_VOA_Instrument.setAtten(40) 
opcVal = Noise_VOA_Instrument.getOPC()                          
td = time.time()-t0
while (opcVal == 0) and (td < timeout):
    opcVal =Noise_VOA_Instrument.getOPC()
    td = time.time()-t0
    time.sleep(0.1)

if td >= timeout:       
    testtime = td
    print("2-Timeout waiting for *OPC? to return 1.  td = %0.2f, OPC = %d"%(td, opcVal))
            
        
print('Test Finished')

#Close communications with all equipments
Osa_Instrument.disconnect()
N7752A_VISA.close()
cfp_obj.disconnect()