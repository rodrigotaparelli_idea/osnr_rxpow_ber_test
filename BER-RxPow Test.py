#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Copyright Idea! - Electronic Systems
#   _____     _               _
#  |_   _|   | |             | |
#    | |   __| |  ___   __ _ | |
#    | |  / _` | / _ \ / _` || |
#   _| |_| (_| ||  __/| (_| ||_|
#  |_____|\__,_| \___| \__,_|(_)
# 
# Name:         Sensitivity
# Purpose:      TxPower step change in order to map RX Sensitivity 
#               by BER/post-FEC
# Created By  : Rodrigo Taparelli - rodrigo.taparelli@idea-ip.com
# Created Date: 27 09 2021
# Release:      1.0.0 - First release
#               1.0.1 - set att. with RX_VOA_attenuation_ch_3_4_start at the end of routine
# =============================================================================

import pyvisa
import time
import numpy as np
from Keysight_N7752A_Driver import *
from yokogawa import *
import pickle
import pandas as pd 
import datetime
import idea_types
import cfp_api.cfp_api as cfp_api
import matplotlib.pyplot as plt

#==================USER inputs==================================================================================
# VOA - Variable Optical Attenuator - Test Params
RX_VOA_attenuation_ch_3_4_start = 20.0     
RX_VOA_attenuation_ch_3_4_end = 30.0      
RX_VOA_attenuation_ch_3_4_inc = 1.0      

NOISE_VOA_attenuation_ch_1_2_start=40.0 # Noise signal attenuation 
                                  

BER_trigger_value_to_change_step_size = 1e-3
New_RX_VOA_attenuation_step = 0.1
#================================================================================================================

# CFP/CFP2 definitions=======================================================================================================
HOSTBOARD_SLOT = 3   # Host Board Slot 
#================================================================================================================

#==================Equipment Addresss/Definitions================================================================
#N7752A Equipment
N7752A_VISA = openInst('TCPIP0::10.24.101.131::inst0::INSTR')
Noise_VOA_Instrument = N7752A(N7752A_VISA,1)
Noise_Power_Monitor_Instrument = N7752A(N7752A_VISA,2)
Rx_VOA_Instrument = N7752A(N7752A_VISA,3)
Rx_Power_Monitor_Instrument = N7752A(N7752A_VISA,4)

#OSA Yokogawa Equipment
YOKOGAWA_AQ6370_IP = '10.24.101.130'
YOKOGAWA_AQ6370_PORT = 10001
YOKOGAWA_AQ6370_USER = 'anonymous'
YOKOGAWA_AQ6370_PWD = ''

#eq and cfp connection=====================================
cfp_obj=cfp_api.CfpApi(hostbrd_slot=HOSTBOARD_SLOT)
print('CFP serial number:')
print(cfp_obj.cfp_serial)
print('CFP Model:')
print(cfp_obj.cfp_model)
Osa_Instrument = Osa(YOKOGAWA_AQ6370_IP, YOKOGAWA_AQ6370_PORT,YOKOGAWA_AQ6370_USER,YOKOGAWA_AQ6370_PWD)      

#Initial configuration values
Noise_VOA_Instrument.setAtten(NOISE_VOA_attenuation_ch_1_2_start) 
time.sleep(1)

#=================================================================================================================

#BER versus receiver sensitivity measurements test main===========================================================        

#list definitions
Rx_VOA_Read_Value=[]
Rx_Power_Monitor_Read_Value=[]  
Rx_Power_Monitor_CFP_Read_Value=[]
# Osa_RX_Power_Monitor_Value=[] 
BER_A_Read_Value=[]
Post_FEC_A_Read_Value=[]
BER_B_Read_Value=[]
Post_FEC_B_Read_Value=[]
  
print('BER versus receiver sensitivity measurements') 
   
timeout=10 #10seconds for command timeout
count=0
RX_VOA_attenuation_Range=np.arange(RX_VOA_attenuation_ch_3_4_start,RX_VOA_attenuation_ch_3_4_end+RX_VOA_attenuation_ch_3_4_inc,RX_VOA_attenuation_ch_3_4_inc)

for RX_VOA_attenuation_set in RX_VOA_attenuation_Range:
    
    t0 = time.time()
    
    
    Rx_VOA_Instrument.setAtten(RX_VOA_attenuation_set) #step by step attenuation
    opcVal = Rx_VOA_Instrument.getOPC()                          #Uses *OPC?
    td = time.time()-t0
    while (opcVal == 0) and (td < timeout):
        opcVal =Rx_VOA_Instrument.getOPC()
        td = time.time()-t0
        time.sleep(0.1)
        
    if td >= timeout:       
        print("Timeout waiting for *OPC? to return 1.  td = %0.2f, OPC = %d"%(td, opcVal))
            
    
    time.sleep(2)
    Rx_VOA_Read_Value.append(Rx_Power_Monitor_Instrument.getAtten())  #read Rx attenuation
    Rx_Power_Monitor_Read_Value.append(Rx_Power_Monitor_Instrument.getOptPower())  #read RX Power by N7752A
    
     #BER and PosfeC Read-------------------------------
    BER_A_Read_Value.append(cfp_obj.corrected_hdfec_ber())
    BER_B_Read_Value.append(cfp_obj.corrected_hdfec_ber_b())
    Post_FEC_A_Read_Value.append(cfp_obj.post_hdfec_ber())
    Post_FEC_B_Read_Value.append(cfp_obj.post_hdfec_ber_b())
    Rx_Power_Monitor_CFP_Read_Value.append(cfp_obj.rx_power())  
    #(attenuation, VOA output power, BER, post-FEC errors)
    attenuation=Rx_VOA_Read_Value[-1]
    VOA_output_power=Rx_Power_Monitor_Read_Value[-1]
    Rx_Mon_P_CFP=Rx_Power_Monitor_CFP_Read_Value[-1]
    BER_A=BER_A_Read_Value[-1]
    BER_B=BER_B_Read_Value[-1]
    postFEC_A=Post_FEC_A_Read_Value[-1]
    postFEC_B=Post_FEC_B_Read_Value[-1]

    st1 = f"Att={attenuation}dB"
    st2 = f"VOA Pout={VOA_output_power:.3e}dBm"
    st3 = f"Rx Mon P={Rx_Mon_P_CFP:.3e}dBm"
    st4 = f"BER_A={BER_A:.3e}"
    st5 = f"post-FEC_BER_A={postFEC_A:.3e}"
    st6 = f"BER_B={BER_B:.3e}"
    st7 = f"post-FEC_BER_B={postFEC_B:.3e}"
    separator = ", "
    info = separator.join([st1,st2,st3,st4,st5,st6,st7])
    print(info)
    
      
    time.sleep(1)
    # new loop in order get more points after trigger BER is achieved
    if ((BER_A >= BER_trigger_value_to_change_step_size) or (BER_B >= BER_trigger_value_to_change_step_size)) and (RX_VOA_attenuation_set < RX_VOA_attenuation_ch_3_4_end):    
        # print('BER passou do limite')
        RX_VOA_attenuation_Range_2=np.arange(RX_VOA_attenuation_set+New_RX_VOA_attenuation_step,
                                             RX_VOA_attenuation_set+RX_VOA_attenuation_ch_3_4_inc-New_RX_VOA_attenuation_step,
                                             New_RX_VOA_attenuation_step)
        # RX_VOA_attenuation_Range_2=RX_VOA_attenuation_Range_2 + New_RX_VOA_attenuation_step
        # print(RX_VOA_attenuation_Range_2)
        for RX_VOA_attenuation_set_2 in RX_VOA_attenuation_Range_2:
            t0 = time.time()
    
    
            Rx_VOA_Instrument.setAtten(RX_VOA_attenuation_set_2) #step by step attenuation
            opcVal = Rx_VOA_Instrument.getOPC()                          #Uses *OPC?
            td = time.time()-t0
            while (opcVal == 0) and (td < timeout):
                opcVal =Rx_VOA_Instrument.getOPC()
                td = time.time()-t0
                time.sleep(0.1)
                
            if td >= timeout:       
                print("Timeout waiting for *OPC? to return 1.  td = %0.2f, OPC = %d"%(td, opcVal))
                    
            
            time.sleep(2)
            Rx_VOA_Read_Value.append(Rx_Power_Monitor_Instrument.getAtten())  #read Rx attenuation
            Rx_Power_Monitor_Read_Value.append(Rx_Power_Monitor_Instrument.getOptPower())  #read RX Power by N7752A
            
             #BER and PosfeC Read-------------------------------
            BER_A_Read_Value.append(cfp_obj.corrected_hdfec_ber())
            BER_B_Read_Value.append(cfp_obj.corrected_hdfec_ber_b())
            Post_FEC_A_Read_Value.append(cfp_obj.post_hdfec_ber())
            Post_FEC_B_Read_Value.append(cfp_obj.post_hdfec_ber_b())
            Rx_Power_Monitor_CFP_Read_Value.append(cfp_obj.rx_power())  
            #(attenuation, VOA output power, BER, post-FEC errors)
            attenuation=Rx_VOA_Read_Value[-1]
            VOA_output_power=Rx_Power_Monitor_Read_Value[-1]
            Rx_Mon_P_CFP=Rx_Power_Monitor_CFP_Read_Value[-1]
            BER_A=BER_A_Read_Value[-1]
            BER_B=BER_B_Read_Value[-1]
            postFEC_A=Post_FEC_A_Read_Value[-1]
            postFEC_B=Post_FEC_B_Read_Value[-1]
        
            st1 = f"Att={attenuation}dB"
            st2 = f"VOA Pout={VOA_output_power:.3e}dBm"
            st3 = f"Rx Mon P={Rx_Mon_P_CFP:.3e}dBm"
            st4 = f"BER_A={BER_A:.3e}"
            st5 = f"post-FEC_BER_A={postFEC_A:.3e}"
            st6 = f"BER_B={BER_B:.3e}"
            st7 = f"post-FEC_BER_B={postFEC_B:.3e}"
            separator = ", "
            info = separator.join([st1,st2,st3,st4,st5,st6,st7])
            print(info)
            time.sleep(1)
            #check postFEC
            if (postFEC_A > 0) or (postFEC_B > 0):
                break
        #check postFEC
        if (postFEC_A > 0) or (postFEC_B > 0):
            print('Acquisition stoped due post-FEC BER trigger')
            break
    
    
#round for 3 digits precision scientific notation
Rx_Power_Monitor_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in Rx_Power_Monitor_Read_Value]
Rx_Power_Monitor_CFP_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in Rx_Power_Monitor_CFP_Read_Value]
BER_A_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in BER_A_Read_Value]
Post_FEC_A_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in Post_FEC_A_Read_Value]
BER_B_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in BER_B_Read_Value]
Post_FEC_B_Read_Value_R=[np.format_float_scientific(num, precision=3) for num in Post_FEC_B_Read_Value]
#Dictionary------------------------------------------------------------
Acquired_data = {
'Att[dB]': Rx_VOA_Read_Value,
'VOA Pout [dBm]': Rx_Power_Monitor_Read_Value_R, #confirmar se é dB ou dBm????
'Rx Mon P [dBm]': Rx_Power_Monitor_CFP_Read_Value_R,
'BER A': BER_A_Read_Value_R,
'Post-FEC BER A': Post_FEC_A_Read_Value_R,
'BER B': BER_B_Read_Value_R,
'Post-FEC BER B': Post_FEC_B_Read_Value_R,
}
# print(Acquired_data)
 
# Store data as .cs and .pkl (Year month day time BER-RxPow-Data)-----
now = datetime.datetime.now()
dt_string = now.strftime("%Y%m%d-%H%M%S")
# print("date and time =", dt_string)

with open("./log/"+dt_string+"_BER-RxPow-Data.pkl", 'wb') as handle:
    pickle.dump(Acquired_data, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
with open("./log/"+dt_string+"_BER-RxPow-Data.pkl", "rb") as f:
    object = pickle.load(f)
    
df = pd.DataFrame(object)
df.to_csv("./log/"+dt_string+"_BER-RxPow-Data.csv", index=False)

# BER vs Power graph plot
fig, axs = plt.subplots(1, 1)

axs.set_xlabel('VOA_RxPow (dBm)')
axs.set_ylabel('Corrected BER')
axs.grid(True)
axs.semilogy(Rx_Power_Monitor_Read_Value,BER_A_Read_Value, '.-', label='Corrected BER A')
axs.plot(Rx_Power_Monitor_Read_Value, BER_B_Read_Value, '.-', label='Corrected BER B')
axs.legend(loc='upper right')

plt.title(f"Module #{cfp_obj.cfp_serial}: VOA_RxPow x Corrected BER") 
plt.savefig("./log/"+dt_string+"_BER-VOA_RxPow-Graph" + '.png')

#Return Rx VOA to start value
Rx_VOA_Instrument.setAtten(RX_VOA_attenuation_ch_3_4_start) 
opcVal = Rx_VOA_Instrument.getOPC()                          
td = time.time()-t0
while (opcVal == 0) and (td < timeout):
    opcVal =Rx_VOA_Instrument.getOPC()
    td = time.time()-t0
    time.sleep(0.1)

if td >= timeout:       
    testtime = td
    print("2-Timeout waiting for *OPC? to return 1.  td = %0.2f, OPC = %d"%(td, opcVal))
            
    
print('Test Finished')  
        
#Close communications with all equipments-----------------------------
Osa_Instrument.disconnect()
N7752A_VISA.close()
cfp_obj.disconnect()