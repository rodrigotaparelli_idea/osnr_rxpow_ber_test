#-------------------------------------------------------------------------------
# Name:        Keysight N7752A Driver
# Purpose:     Instrument Control of Keysight N7752A Dual Atten + Dual OPM
# Release:     1.1
#
# Known Bugs:  1) Does not check for attenuator 'ExP' (out of range) when
#                 operating in Power Control mode.  Need to add similar
#                 timeout feature to ensure ExP is not set when either timeout
#                 is set or OPC is complete
#              2) Similar for Power Meter, ExP will set when power is too high
#                 Need to add a check for ExP on power meters and throw
#                 an error or exception, or change the power meter range
#
# Author:      CMacGregor
#
# Created:     07-May-2019
# Copyright:   (c) CMacGregor 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import pyvisa
import time
import numpy as np





################################################################################
################################################################################
##
## These functions are not part of the Keysight driver class
##
################################################################################
################################################################################

def openInst(instname):
    """Opens VISA session to instname ane returns the session ID.
        Args:
            instname (str) -- string defining the VISA Instrument Name (such as 'TCPIP0::xx.xx.xx.xx::inst0::INSTR')
        Returns:
            inst    (inst) -- instance for the connection
    """
    rm = pyvisa.ResourceManager()
    inst = rm.open_resource(instname)
    return inst

def getResources():
    """Gets a list of all available resources
        Args:
            none
        Returns:
            availRsrcs (lib) -- library of available resources
    """
    rm = pyvisa.ResourceManager()
    availRsrcs = rm.list_resources()
    return availRsrcs

################################################################################
################################################################################
##
## N7752A Driver Class
##
################################################################################
################################################################################

class N7752A:
    """Driver for Keysight N7752A Dual Attenuator Power Meter.  Requires a VISA connection (inst) to communicate
    with the N7752A and a port ID related to the slot/port on the N7752.
    N7752A Class is probably not threadsafe (unless there are threadsafe features built into pyVISA).
    If you have multiple instruments from the same N7752A running in different
    threads, you could have problems.
    """
    def __init__(self,inst,port):
        self.inst = inst
        if port == 1 or port == 2:
            self.inp = 1
            self.outp = 2
        elif port == 3 or port == 4:
            self.inp = 3
            self.outp = 4
        elif port == 5:
            self.inp = 5
            self.outp = 5
        elif port == 6:
            self.inp = 6
            self.outp = 6
        else:
            print("Error: Unrecognized port value.  Port = %d"%port)
            raise ValueError
        return
    
    
    
    def getSTB(self):
        """Returns Status Byte information.
        returns:
            val (int)       -- Status Byte value
            statarr (array) -- Array of strings containing description of each bit that is set in status byte
        """
        val = int(self.inst.query("*STB?"))
        #print("Value = %s"%val)
        statarrkey = [[7,'OSB(7)'], [5,'ESB(5)'], [4,'MAV(4)'],[3,"QSB(3)"],[2,"EVT/ERR_QUEUE(2)"]]
        statarr = []
        for i in range(len(statarrkey)):
            if (val>>statarrkey[i][0])&0x1:
                statarr.append(statarrkey[i][1])
        return (val,statarr)

    def getOPC(self, slot = 0):
        """Returns Operation Complete (OPC).  If slot is 1 (True), the slot-specific command "SLOT[n]:OPC?" is used
        where n = self.inp.  If slot is 0 (False), the generic *OPC? command is used.
        args:
            slot (int, 0) -- 0 (use generic command) | 1 (use slot-specific command)
        returns:
            val (int)     -- 0 (busy) | 1 (operation complete)
        """
        if slot:
            cmdstr = "SLOT%d:OPC?"%self.inp
        else:
            cmdstr = "*OPC?"
        val = int(self.inst.query(cmdstr))
        return val

    def waitforOPC(self,slot = 0,timeout = 10.0):
        """Queries OPC until OPC = 1 (complete) or delay exceeds timeout (default 10s)
        Uses the "getOPC()" function - see that function's help for additional information.
        args:
            slot (int, 0)       -- 0 (use generic command) | 1 (use slot-specific command)
            timeout (float, 10) -- timeout period in seconds (default 10s)
        returns:
            val (int)           -- 0 (operation incomplete) | 1 (operation complete)
            to (float)          -- total delay in seconds
        """
        t0 = time.time()
        val = 0
        to = time.time()-t0
        while val==0 and to < timeout:
            val = self.getOPC(slot)
            to=time.time()-t0
        if to>=timeout:
            print("Error: OPC did not set before timeout.  delay = %0.3f"%to)
        return (val,to)

    def getESR(self):
        """Returns Event Status Register (ESR) information.
        returns:
            val (int)       -- ESR value
            statarr (array) -- Array of strings containing description of each bit that is set in ESR
        """
        val = int(self.inst.query("*ESR?"))
        #print("Value = %s"%val)
        statarrkey = [[7,'PWR_ON(7)'], [5,'CMD_ERR(5)'], [4,'EXC_ERR(4)'],[3,"DEV_ERR(3)"],[2,"QRY_ERR(2)"],[0,"OPC(0)"]]
        statarr = []
        for i in range(len(statarrkey)):
            if (val>>statarrkey[i][0])&0x1:
                statarr.append(statarrkey[i][1])
        return (val,statarr)

    def getError(self,maxlen = 100):
        """Returns an array of errors from the Error/Event Queue until no messages are left in the queue.
        Limits number of reported errors to maxlen.
        SCPI Command:
            :SYSTem:ERRor?
        Args:
            maxlen (int, 100) -- total number of error messages to read from queue
        Returns:
            errarray (array)  -- Error Message Array (errarray[i] = [errcode,errstr])
        """
        errarray = []
        stb,delme = self.getSTB()
        errstat = (stb>>2)&1
        while errstat and len(errarray)<maxlen:
            reterr = self.inst.query("SYST:ERR?")
            errcode = int(reterr.split(',')[0])
            errstr = reterr.split('"')[1]
            errarray.append([errcode,errstr])
            stb,delme = self.getSTB()
            errstat = (stb>>2)&1
        reterr = self.inst.query("SYST:ERR?")
        errcode = int(reterr.split(',')[0])
        errstr = reterr.split('"')[1]
        errarray.append([errcode,errstr])
        return errarray

    def errorReportGen(self):
        """ ############  WORK IN PROGRESS  ###################
        need to figure out format for the error report
        maybe best to just output a string block
        currently just prints info to console, does not return
        anything.
        """
        stb,delme = self.getSTB()
        while stb != 0:
            print("STB = %s"%stb)
            if (stb>>2)&1:
                errarray = self.getError()
                print("### Error/Event Queue ###")
                print(errarray)
            if (stb>>3)&1:
                qsbcond = self.inst.query("stat:ques:cond?")
                qsbevnt = self.inst.query("stat:ques:event?")
                print("### Questionable Status Summary ###")
                print("qsbcond = %s"%qsbcond)
                print("qsbevnt = %s"%qsbevnt)
            if (stb>>4)&1:
                print("### Message Available ###")
                # may want to just do a self.inst.read() since there's a message available
                mav = 1
                print(mav)
            if (stb>>5)&1:
                print("### Event Status Register ###")
                esr = self.getESR()
                print(esr)
            if (stb>>7)&1:
                print("### Operation Status Summary ###")
                opscond = self.inst.query("stat:oper:cond?")
                opsevnt = self.inst.query("stat:oper:event?")
                print("opscond = %s"%opscond)
                print("opsevnt = %s"%opsevnt)
            stb,delme = self.getSTB()
        return

    def setAtten(self,val):
        """Sets attenuation of selected attenuator chan to value val.  Minimum precision is 0.01dB.
        SCPI command:
        :INPut[n][:CHANnel[m]]:ATTenuation<wsp><value>[dB] | MIN | DEF | MAX
        Args:
            val (int or str) -- can be a numerical attenuation value in dB or
                                a string "MIN", "DEF", or "MAX"
        Returns:
            statcode (int)   -- PyVISA status code
        """
        if isinstance(val,float) or isinstance(val,int):
            cmdstr = "INP%d:ATT %0.2fdB"%(self.inp,val)
        elif val == "MIN" or val == "MAX" or val == "DEF":
            cmdstr = "INP%d:ATT %s"%(self.inp,val)
        else:
            print("Error: Unrecognized val.  val = %s"%val)
            raise ValueError
        ret_bytes = self.inst.write(cmdstr)
        # if int(statcode) != 0:
        #     print("Warning: Write status did not return 0.  statcode = %s"%statcode)
        opc,to = self.waitforOPC(self.inp)
        #print("OPC = %d, to = %s"%(opc,to))
        return ret_bytes

    def getAtten(self):
        """Get current attenuation setting
        SCPI Command:
        :INPut[n][:CHANnel[m]]:ATTenuation?<wsp> MIN | DEF | MAX
        Returns:
            output (float) -- attenuation setting in dB
        """
        atten = self.inst.query("INP%d:ATT?"%self.inp)
        return float(atten)

    def setAttnPwr(self,pwr,unit='DBM',timeout = 10.0):
        """Sets attenuator to output power mode (if it is not already) and sets the desired output power level.
        SCPI command:
        :OUTPut[n][:CHANnel[m]]:POWer<wsp><value>[PW | NW | UW | MW | W | DBM ] | MIN | MAX | DEF
        Args:
            pwr (int or str)  -- Output power level or a string
            unit (str, 'DBM') -- Power unit (see SCPI command above for valid values)
            timeout (num, 10) -- timeout in sec.  If timeout > 0, waits until OPC = 1 or timeout.
                                 If timeout <= 0, does not check for OPC.
        Returns:
            statcode (int)    -- PyVISA status code
        Warning: Setting output power may not achieve OPC within timeout if the input signal is unstable.
        """
        unit = str.upper(unit)
        if isinstance(pwr,float) or isinstance(pwr,int) or isinstance(pwr,np.int32):
            if (unit=='DBM' or unit=='PW'  or unit=='NW'  or unit=='UW'  or unit=='MW'  or unit=='W'):
                cmdstr = "OUTP%d:POW %0.2e%s"%(self.outp,pwr,unit)
            else:
                print("Error: unrecognized unit value. unit = %s"%unit)
                raise ValueError
        elif isinstance(pwr,str):
            if (val == 'MIN' or val == 'DEF' or val == 'MAX'):
                cmdstr = "OUTP%d:POW %s"%(self.outp,pwr)
            else:
                print("Error: val (if type str) must be 'MIN', 'DEF', or 'MAX'.  val = %s"%val)
                raise ValueError
        else:
            print("Error: Unrecognized value (must be float, int, or str). val = %s"%pwr)
            raise ValueError
        attmode = self.getAttnPwrCntrlMode()
        if attmode==0:
            val = self.setAttnPwrCntrlMode(1)
        ret_bytes = self.inst.write(cmdstr)
        # if int(statcode) != 0:
        #     print("Warning: Write Statcode not 0. statcode = %d"%statcode)
        if timeout > 0:
            opc,to = self.waitforOPC(self.inp,timeout)
        return ret_bytes

    def setAttnSpeed(self,val=1000):
        """Sets attenuation speed.  Attenuation speeds > 80dB/s are set to 1000dB/s automatically by instrument
        SCPI command:
        :INPut[n][:CHANnel[m]]:ATTenuation:SPEed<wsp><value> | MIN | MAX | DEF
        Args:
            val (int or str) -- can be a speed value in dB/s or
                                a string "MIN", "DEF", or "MAX"
        Returns:
            statcode (int)   -- PyVISA status code
        """
        if isinstance(val,float) or isinstance(val,int):
            cmdstr = "INP%d:ATT:SPE %0.1f"%(self.inp,val)
        elif val == "MIN" or val == "MAX" or val == "DEF":
            cmdstr = "INP%d:ATT:SPE %s"%(self.inp,val)
        else:
            print("Error: Unrecognized val.  val = %s"%val)
            raise ValueError
        ret_bytes = self.inst.write(cmdstr)
        # if int(statcode) != 0:
        #     print("Warning: Write status did not return 0.  statcode = %s"%statcode)
        return ret_bytes

    def setAttnPwrCntrlMode(self,mode):
        """Enable or disable power control mode (1 enable, 0 disable)
        :OUTPut[n][:CHANnel[m]]:APMode<wsp><OFF(0) | ON(1)>
        Args:
            mode (int)      -- 1 (ON) | 0 (OFF)
        Returns:
            statcode (int)  -- pyVISA status code
        """
        cmdstr = "OUTP%d:APM %d"%(self.outp,mode)
        ret_bytes = self.inst.write(cmdstr)
        return ret_bytes

    def getAttnPwrCntrlMode(self):
        """Get Power Control Mode for device
        :OUTPut[n][:CHANnel[m]]:APMode?
        Returns:
            apm  (int) -- 1 (ON) | 0 (OFF)
        """
        cmdstr = "OUTP%d:APM?"%self.outp
        apm = self.inst.query(cmdstr)
        return int(apm)

    def setOutpEnable(self,mode,timeout = 10.0):
        """Opens or shuts the shutter (1 open, 0 shut)
        SCPI Command:
        :OUTPut[n][:CHANnel[m]][:STATe]<wsp>OFF(0) | ON(1)
        Args:
            mode (int)          -- 1 (ON) | 0 (OFF)
            timeout (float, 10) -- timeout in seconds
        Returns:
            statcode (int)      -- pyVISA status code
        """
        cmdstr = "OUTP%d:STAT %d"%(self.outp,mode)
        ret_bytes = self.inst.write(cmdstr)
        # if int(statcode) != 0:
        #     print("Warning: Write Statcode not 0. statcode = %d"%statcode)
        opc,to = self.waitforOPC(self.outp,timeout)
        if opc == 0:
            print("Warning: OPC timeout. OPC = %d, to = %s"%(opc,to))
        return ret_bytes

    def getOptPowerUnit(self):
        """Get optical power units for selected output
        SCPI Command:
        :OUTPut[n][:CHANnel[m]]:POWer:UNit?
        Returns:
            output (int) -- 0 (dBm) | 1 (W)
        """
        unit = self.inst.query("OUTP%d:POW:UN?"%self.outp)
        return int(unit)

    def getOptPower(self,trig = 0):
        """Read optical power from selected power meter.  Can be triggered or untriggered.
        SCPI Command:
        Untriggered: :READ[n][:CHANnel[m]][:SCALar]:POWer[:DC]?
        Triggered:   :FETCh[n][:CHANnel[m]][:SCAlar]:POWer[:DC]?
        Args:
            trig (int, 0)  -- trigger power measurement (1) | read current value (0 - default)
        Returns:
            output (float) -- optical power level
        Note: Attenuator measurements cannot be triggered.  Function handles this by revaluing trig.
        """
        if self.inp < 5 or self.outp < 5:
            trig = 0
        if trig==1:
            t0 = time.time()
            ret_bytes = self.inst.write("INIT%d:CONT 0"%self.inp)
            ret_bytes = self.inst.write("INIT%d:IMM"%self.inp)
            optpwr = self.inst.query("FETC%d:POW?"%self.inp)
            ret_bytes = self.inst.write("INIT%d:CONT 1"%self.inp)
        elif trig==0:
            optpwr = self.inst.query("READ%d:POW?"%self.outp)
        else:
            print("Error: Unrecognized trig value. trig = &s"%trig)
            raise ValueError
        return float(optpwr)

    #################################################################
    ## Unfinished functions
    #################################################################

    def setPwrUnits(inst,unit):
        return

    def setPwrMetWaveLen(self,val):
        return

    def setOptPwrAvgTime(self,val):
        return

    def setOffsets(self,pwroff,attoff=None):
        return

'''
example use for N7752A class :

You have a test system that has uses both attenuators and both power monitors.
  -The first attenuator (ports 1 and 2) will be called atten1
  -The second attenuator (ports 3 and 4) will be called atten2
  -The first power detector (port 5) will be called pmon1
  -The second power detector (port 6) will be called pmon2

First, create a VISA Session to the N7752A device:
    N7752A_VISA = openInst('TCPIP0::10.50.31.105::inst0::INSTR')
That VISA session can be used to create instrument objects that refer to each module/slot in the N7752A
    atten1 = N7752A(N7752A_VISA,1)
    atten2 = N7752A(N7752A_VISA,3)
    pmon1 = N7752A(N7752A_VISA,5)
    pmon2 = N7752A(N7752A_VISA,6)

After you've created your instruments, you can use the N7752A class to interact with the device:
    aseatten.setAtten(10.531) will set the INP1 attenuator to 10.53dB
    rxatten.setAttnPwr(5.128) will set the INP3 attenuator to power control mode (if not already set),
        and set the output power level to 5.13dBm.
    txmon.getOptPwr() will read the optical power measured on INP5

Make sure you close the VISA sessions when the program ends:
    N7752A_VISA.close() or alternatively,
    atten1.inst.close() in this case, if you use the pyVISA close() method on the 'inst',
    that will close the VISA session to the N7752A module for all objects associated with the N7752A
    as well.

'''



def debug(attenspeed):
    """debug function for this module.
    Opens a VISA session to a N7752A, creates objects for each of the devices.
    Sets the attenuation speed
    """
    try:
        #open VISA session and instrument objects
        N7752A_VISA = openInst('TCPIP0::10.24.101.134::inst0::INSTR')
        atten1 = N7752A(N7752A_VISA,1)
        atten2 = N7752A(N7752A_VISA,3)
        pmon1 = N7752A(N7752A_VISA,5)
        pmon2 = N7752A(N7752A_VISA,6)


        print("Setting attenuation speed to 1000dB/s")
        atten1.setAttnSpeed(float(attenspeed))
        t0=time.time()
        print("Enabling output on Atten1")
        atten1.setOutpEnable(1,30)
        print("Enabling Output on Atten1 took %0.3fs"%(time.time()-t0))
        t0 = time.time()
        atten1.setAtten(0)
        print("Setting attenuation 0dB took %0.3fs"%(time.time()-t0))
        t0 = time.time()
        atten1.setAtten(30)
        print("Setting attenuation 30dB took %0.3fs"%(time.time()-t0))
        t0 = time.time()
        atten1.setAtten(0)
        print("Setting attenuation 0dB took %0.3fs"%(time.time()-t0))
        t0 = time.time()
        atten1.setOutpEnable(0)
        print("Disabling Output on Atten1 took %0.3fs"%(time.time()-t0))

        print("Output Power atten1 = %0.2f"%(atten1.getOptPower(1)))
        print("Output Power atten2 = %0.2f"%(atten2.getOptPower(1)))
        print("Output Power pmon1 = %0.2f"%(pmon1.getOptPower(1)))
        print("Output Power pmon2 = %0.2f"%(pmon2.getOptPower(1)))


    except ValueError:
        # Error Handler goes in here somewhere
        print("ValueError was raised")
    except pyvisa.VisaIOError:
        atten1.errorReportGen()
    finally:
        atten1.errorReportGen()
        print("Closing connection to N7752A")
        N7752A_VISA.close()
    return
#%%
# if __name__== "__main__":
#     debug(1000)



