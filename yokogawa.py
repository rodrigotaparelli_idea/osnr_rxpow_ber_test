"""
Yokogawa AQ6370 equipment API

@copyright Idea! Electronic Systems
@author Diogo de Azevedo Motta <diogo.motta@idea-ip.com>
@author Felipe Bizerra Fideles <felipe.fideles@idea-ip.com>
@author Breno Alves <breno.alves@idea-ip.com>
"""

import time
import socket
import numpy as np
from scipy.constants import c


class OsnrImmStrategy: 
    def __init__(self) -> None:
        pass
    @staticmethod
    def get_osnr(osa_obj):
        'Set OSNR measuring mode and return the calculated osnr'
        send = ':CALCULATE:PARAMETER:WDM:SPOWER PEAK\n'
        osa_obj.s.send(send.encode('utf-8'))
        send = ':CALC:IMM\n'
        osa_obj.s.send(send.encode('utf-8'))
        data_vector = osa_obj.data()
        data_vector = data_vector.decode()
        data_float = np.zeros(len(data_vector.split(','))
                              )  # Data comes as CSV string
        count = 0
        # Separating the CSV numbers and converting to float
        for i in data_vector.split(','):
            data_float[count] = float(i)
            count = count + 1
        return data_float
              
class OsnrDataStrategy: 
    def __init__(self) -> None:
        pass
    
    @staticmethod
    def get_osnr(osa_obj):
        'Queries the SNR value from the last time WDM analysis was executed.'
        try:  
            send = ':CALCulate:IMMediate\n'
            osa_obj.s.send(send.encode('utf-8'))
            osa_obj.check_operation()
            send = ':CALCulate:DATA:CSNR?\n'
            osa_obj.s.send(send.encode('utf-8'))
            
            data_vector = osa_obj.s.recv(osa_obj.sampling_point).decode('utf-8')

            data_float = np.zeros(len(data_vector.split(','))
                                  )  # Data comes as CSV string
            count = 0
            # Separating the CSV numbers and converting to float
            for i in data_vector.split(','):
                data_float[count] = float(i)
                count = count + 1

        except Exception as e:
            raise e

        return data_float

OSNR_STRATEGY_DICT = {
    'imm': OsnrImmStrategy,
    'data':  OsnrDataStrategy, 
}


class Osa(object):    
    '''Class for communication with Yokogawa Optical Spectrum Analyzer'''

    MAX_SAMPLING_POINT = 50001

    def __init__(self, host_ip, port, usr, pwd, default_config=False):
        self.s = socket.socket()

        self.s.connect((host_ip, port))
        time.sleep(0.6)

        send = 'open "' + usr + '"\n'
        self.s.send(send.encode('utf-8'))
        self.s.recv(self.MAX_SAMPLING_POINT)
        time.sleep(0.6)

        send = pwd + '\n'
        self.s.send(send.encode('utf-8'))
        self.s.recv(self.MAX_SAMPLING_POINT)
        time.sleep(0.6)
        
        self.sampling_point = self.get_sampling_point()
        
        if default_config:
            self.set_wavelength_x()
            self.set_wavelength_center_nm(1550)
            self.set_wavelength_span_nm(40)
            self.set_wavelength_resolution_nm(0.02)
            self.abort_sweep()
        

    def __del__(self):
        self.s.close()


    def get_sampling_point(self):
        'Get OSA sampling point. '

        send = ':SENSE:SWEEP:POINTS?\n'
        self.s.send(send.encode('utf-8'))
        sp = self.s.recv(self.MAX_SAMPLING_POINT)
        
        return int(sp)

    def set_wavelength_center_nm(self, wave_center):
        'Set wavelenght center in nm'
        send = ':SENSe:WAVelength:CENTer {:f}NM\n'.format(wave_center)
        self.s.send(send.encode('utf-8'))

    def set_frequency_center_ghz(self, freq_center):
        'Set frequency center in GHz'
        send = ':SENSe:WAVelength:CENTer {:f}GHZ\n'.format(freq_center)
        self.s.send(send.encode('utf-8'))
        

    def data(self):
        'Read Data'
        send = ':CALCulate:DATA?\n'
        self.s.send(send.encode('utf-8'))
        return self.s.recv(self.sampling_point)

    def set_frequency_x(self):
        'Set X unit in frequency'
        send = ':UNIT:X FREQuency\n'
        self.s.send(send.encode('utf-8'))

    def set_wavelength_x(self):
        'Set X unit in wavelength'
        send = ':UNIT:X WAVelength\n'
        self.s.send(send.encode('utf-8'))

    def start_sweep(self):
        'Start sweep'
        send = ':INITiate\n'
        self.s.send(send.encode('utf-8'))

    def abort_sweep(self):
        'Stop sweep'
        send = ':abort\n'
        self.s.send(send.encode('utf-8'))

    def set_sweep_mode(self, mode):
        'Set sweep mode: '
        send = {'single': ':INITiate:SMODe SINGle\n',
                'repeat': ':INITiate:SMODe REPeat\n',
                'auto': ':INITiate:SMODe AUTO\n'}[mode]
        self.s.send(send.encode('utf-8'))

    def set_maximum_marker(self):
        'Put a maximum marker on signal'
        send = ':CALCulate:MARKer:MAXimum\n'
        self.s.send(send.encode('utf-8'))

    def get_x_value(self, idx):
        'Get X value of marker'
        send = ':CALCulate:MARKer:X? {:d}\n'.format(idx)
        self.s.send(send.encode('utf-8'))
        return float(self.s.recv(self.sampling_point))

    def set_wavelength_span_nm(self, value):
        'Set sweep span in nm'
        self.set_wavelength_x()
        send = ':SENSe:WAVelength:SPAN {:f}NM\n'.format(value)
        self.s.send(send.encode('utf-8'))

    def set_wavelength_resolution_nm(self, value):
        'Set bandwidth resolution in nm'
        self.set_wavelength_x()
        send = ':SENSe:BWIDth:RESolution {:f}NM\n'.format(value)
        self.s.send(send.encode('utf-8'))

    def get_operation_status(self):
        'Get the content of the operation status Event register'
        send = ':STATus:OPERation:EVENt?\n'
        self.s.send(send.encode('utf-8'))
        return float(self.s.recv(self.sampling_point))

    def check_operation(self):
        'Check if operation has been completed'
        k = 0
        send = ':STAT:OPER:EVEN?\n'
        t1 = time.time()
        t2 = 0
        while k == 1:
            self.s.send(send.encode('utf-8'))
            time.sleep(0.1)
            k = self.s.recv(self.sampling_point)
            t2 = time.time()
            if (t2 - t1 > 10):
                k = 0
                break
        return k

    def set_sense(self, mode):
        'Set the measurement sensitivity'
        send = {'normal auto': ':INITiate:SMODe NAUT\n',
                'normal': ':INITiate:SMODe NORM\n',
                'high': ':INITiate:SMODe HIGH1\n'}[mode]
        self.s.send(send.encode('utf-8'))

    def peak_to_ref_level(self):
        'Set peak level as reference'  # and returns the reference power'
        send = ':CALC:MARK:MAX:SRL\n'
        self.s.send(send.encode('utf-8'))
        time.sleep(0.4)
#        send = 'CALC:CAT POW'
#        self.s.send(send.encode('utf-8'))
#        send = 'CALC:IMM'
#        self.s.send(send.encode('utf-8'))
#        return self.data()

    def max_lambda(self):
        'Find and return the lambda relative to the maximum power'
        # Set the search function to multi search
        send = ':CALCulate:MARKer:MSEarch 1\n'
        self.s.send(send.encode('utf-8'))
        send = ':CALCulate:MARKer:AUTO 1\n'  # Set the auto search function
        self.s.send(send.encode('utf-8'))
        # Set the sort order of the multi search detection list
        send = ':CALCulate:MARKer:MSEarch:SORT 1\n'
        self.s.send(send.encode('utf-8'))
        send = ':CALCulate:MARKer:MSEarch:THResh 10\n'  # Set the multi search threshold
        self.s.send(send.encode('utf-8'))
        send = ':CALCULATE:MARKER:X? 0\n'  # Queries the lambda of the marker
        self.s.send(send.encode('utf-8'))
        return float(self.s.recv(self.sampling_point))

    def get_smsr(self):
        'Set SMSR measuring mode and return the calculated smsr'
        'Returns a vector containing, respectively, the peak lambda, peak level, 2nd peak lambda, 2nd peak level, delta lambda and delta level (SMSR)'
        send = ':CALCULATE:PARAMETER:SMSR:MODE SMSR1\n'
        self.s.send(send.encode('utf-8'))
        send = ':CALC:IMM\n'
        self.s.send(send.encode('utf-8'))
        data_vector = self.data()
        data_vector = data_vector.decode()
        data_float = np.zeros(len(data_vector.split(','))
                              )  # Data comes as CSV string
        count = 0
        # Separating the CSV numbers and converting to float
        for i in data_vector.split(','):
            data_float[count] = float(i)
            count = count + 1
        return data_float
    
    def get_power(self):
        'Set POWER measuring mode and return the Total Power'
        'Returns a vector containing total power'
        
        send = ':CALCULATE:CATEGORY POWER\n'
        self.s.send(send.encode('utf-8'))
        send = ':CALC:IMM\n'
        self.s.send(send.encode('utf-8'))
        data_vector = self.data()
        
        data_vector = data_vector.decode() # Data comes as CSV string
        data_float = np.zeros(len(data_vector.split(','))) 
        count = 0
        # Separating the CSV numbers and converting to float
        for i in data_vector.split(','):
            data_float[count] = float(i)
            count = count + 1
        return data_float
    
    
    def smsr_operation(self):
        'Procedure to measure the SMSR'
        self.set_wavelength_resolution_nm(0.02)
        self.set_sweep_mode('single')
        self.start_sweep()
        self.check_operation()
        self.peak_to_ref_level()
        lmbda = self.max_lambda()

        # Put here the OSA check connection

        self.set_sense('high')
        # Zoom in at maximum lambda with  3nm span
        self.set_wavelength_center_nm(lmbda * 1e9)
        self.set_wavelength_span_nm(6)
        self.start_sweep()
        self.check_operation()
        smsr = self.get_smsr()

        # Put here the OSA check connection

        self.set_sweep_mode('repeat')
        self.set_sense('normal')

        return smsr

    def get_trace_x(self, trace):
        'Get trace x axis data of the specified trace: '
        tr = {'A': 'TRA',
              'B': 'TRB',
              'C': 'TRC',
              'D': 'TRD',
              'E': 'TRE',
              'F': 'TRF',
              'G': 'TRG'}[trace]

        send = ':TRACE:X? {}\n'.format(tr)
        self.s.send(send.encode('utf-8'))
        raw_data = self.s.recv(self.sampling_point)
        while raw_data[-2:] != b'\r\n':
            raw_data += self.s.recv(self.sampling_point)

        raw_data = raw_data.decode()
        data_float = np.zeros(len(raw_data.split(','))
                              )  # Data comes as CSV string
        count = 0
        # Separating the CSV numbers and converting to float
        for i in raw_data.split(','):
            data_float[count] = float(i)
            count += 1
        return data_float

    def get_trace_y(self, trace):
        'Get trace y axis data of the specified trace: '
        tr = {'A': 'TRA',
              'B': 'TRB',
              'C': 'TRC',
              'D': 'TRD',
              'E': 'TRE',
              'F': 'TRF',
              'G': 'TRG'}[trace]

        send = ':TRACE:Y? {}\n'.format(tr)
        self.s.send(send.encode('utf-8'))
        raw_data = self.s.recv(self.sampling_point)
        while raw_data[-2:] != b'\r\n':
            raw_data += self.s.recv(self.sampling_point)

        raw_data = raw_data.decode()
        data_float = np.zeros(len(raw_data.split(','))
                              )  # Data comes as CSV string
        count = 0
        # Separating the CSV numbers and converting to float
        for i in raw_data.split(','):
            data_float[count] = float(i)
            count += 1
        return data_float
    
    def set_noise_area(self, val):
        send = f':CALCULATE:PARAMETER:WDM:NAREA {val:.2f}NM\n'
        self.s.send(send.encode('utf-8'))

    def get_osnr_wavelenght(self):
        'Queries the wavelength value of the OSNR(WDM), WDM, EDFA-NF, WDM FIL-PK, or WDM FIL-BTM analysis results.'
        try:  
            send = ':CALCulate:DATA:CWAVelengths?\n'
            self.s.send(send.encode('utf-8'))
            data_vector = self.s.recv(self.sampling_point).decode('utf-8')

            data_float = np.zeros(len(data_vector.split(','))
                                  )  # Data comes as CSV string
            count = 0
            # Separating the CSV numbers and converting to float
            for i in data_vector.split(','):
                data_float[count] = float(i)
                count = count + 1
        except Exception as e:
            raise e
       
        response = np.asarray(data_float)
        return c/((response*1e-9))

    def get_osnr(self, algorithm='imm'):
        try:
            osnr_algorithm = OSNR_STRATEGY_DICT[algorithm]
        except KeyError:
            raise KeyError(f"Protocols dict do not have {algorithm} algorithm. Options = [{OSNR_STRATEGY_DICT.keys()}]")

        return osnr_algorithm.get_osnr(self)

    
    def osnr_operation(self, *lmbda, power_threshold=None,resolution=0.1,bw=100):
        'Procedure to measure the OSNR'
        self.set_wavelength_resolution_nm(resolution)
        self.set_sweep_mode('single')
        self.start_sweep()
        self.check_operation()
        self.peak_to_ref_level()
        
        if len(lmbda) > 1:
            raise TypeError("osnr_operation()) expected at most 1 argument, got %d"
                            % (len(lmbda) + 1))
        elif len(lmbda) == 1:
            lmbda = lmbda[0]/1e9
        else:
            lmbda = self.max_lambda()    

        self.set_sense('high')
        # Zoom in at maximum lambda with  3nm span
        self.set_wavelength_center_nm(lmbda * 1e9)
        self.set_wavelength_span_nm(3)
        self.start_sweep()
        self.check_operation()
        
        if power_threshold is not None:
            pwr = self.get_power()
            if pwr[0] < power_threshold:
                return None
        
        signal_bw = bw # GHz
        noise_area = (signal_bw*(lmbda*1e9)**2)/(2*c)
        self.set_noise_area(noise_area*1.1) # Noise Area Calculated + 10%
        osnr = self.get_osnr(algorithm='imm')
    
        self.set_sweep_mode('repeat')
        self.set_sense('normal')

        return osnr

    def set_attenuation(self, mode):
        'Set OSA internal attenuation value: '
        send = {'on': ':SENSe:SETting:ATTenuator ON\n',
                'off': ':SENSe:SETting:ATTenuator OFF\n'}[mode]
        self.s.send(send.encode('utf-8'))
    
    def set_connector_type(self, connector):
        'Set connector type (PC, APC, etc) to correctly define OSA internal setup loss: '
        
    def status_clear(self):
        'Clear the OSA internal buffer: '
        send = '*CLS'
        self.s.send(send.encode('utf-8'))
    
    def set_level_shift(self, value):
        'Set level shifting in dB'
        send = ':SENSe:CORRection:LEVel:SHIFt {:f}DB\n'.format(value)
        self.s.send(send.encode('utf-8'))
        
    def get_level_shift(self, value):
        'Get level shifting in dB'
        send = ':SENSe:CORRection:LEVel:SHIFt?\n'
        self.s.send(send.encode('utf-8'))
        return float(self.s.recv(self.sampling_point))  


    def disconnect(self):
        'Disconnect'
        self.s.close() 
        
        
